package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis"
)

type Event struct {
	Code string `json:"code"`
	Evt  string `json:"evt"`
	Addr string `json:"addr"`
}

func init_redis() (rdb *redis.Client) {
	rdb = redis.NewClient(&redis.Options{
		Addr:         fmt.Sprintf("%s:%s", REDISHOST, REDISPORT),
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     3,
		PoolTimeout:  30 * time.Second,
	})
	_, err := rdb.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	return rdb
}

func xread(rdb *redis.Client, pool *Pool) {
	var msg Message
	for {
		res, err := rdb.XRead(&redis.XReadArgs{
			Streams: []string{Redis_stream, "$"},
			Count:   0,
			Block:   0,
		}).Result()
		if err != nil {
			log.Fatal(err)
		}
		xstream := res[0]
		xmessage := xstream.Messages[0]
		values := xmessage.Values
		jvalues, _ := json.Marshal(values)

		evt := Event{}
		json.Unmarshal(jvalues, &evt)

		msg.Type = 1
		msg.Data = evt
		pool.Broadcast <- msg
	}
}

func xrev(rdb *redis.Client) (evts []Event) {

	res, err := rdb.XRevRangeN(Redis_stream, "+", "-", 4).Result()
	if err != nil {
		log.Fatal(err)
	}
	lenres := len(res)

	for idx, _ := range res {
		xmessage := res[lenres-1-idx]
		values := xmessage.Values
		jvalues, _ := json.Marshal(values)
		evt := Event{}
		json.Unmarshal(jvalues, &evt)
		evts = append(evts, evt)
	}
	return evts
}
